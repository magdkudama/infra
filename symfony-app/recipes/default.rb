package 'nginx'

directory '/etc/nginx' do
  owner 'root'
  group 'root'
  mode '0755'
end

directory '/var/log/nginx' do
  mode 0755
  owner 'nginx'
  action :create
end

%w{sites-available sites-enabled conf.d}.each do |dir|
  directory File.join('/etc/nginx', dir) do
    owner 'root'
    group 'root'
    mode 0755
  end
end

%w{nxensite nxdissite}.each do |nxscript|
  template "/usr/sbin/#{nxscript}" do
    source "#{nxscript}.erb"
    owner "root"
    group "root"
    mode 0755
  end
end

template "nginx.conf" do
  path "#{node[:nginx][:dir]}/nginx.conf"
  source "nginx.conf.erb"
  owner "root"
  group "root"
  mode 0644
end

file '/etc/nginx/sites-available/default' do
  action :delete
  only_if { File.exists?("/etc/nginx/sites-available/default") }
end

service "nginx" do
  action [ :enable, :start ]
end